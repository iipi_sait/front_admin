// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    runtimeConfig: {
        public: {
            apiBaseUrl: 'NUXT_PUBLIC_API_BASE_URL',
        }
    },
    css: ['~/assets/css/main.css','~/assets/css/admin.css','~/assets/css/modal.css','~/assets/css/formui.css'],
    devtools: { enabled: true },
    buildModules: ['@nuxt/vuex',"@pinia/nuxt"],
    modules: ["@pinia/nuxt","@pinia/nuxt"]

})
